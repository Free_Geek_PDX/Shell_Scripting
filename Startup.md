# Student (Do before class starts)

## Get scripts
To follow along with the scripts and to have them available later go to
 - https://gitlab.com/Free_Geek_PDX/Shell_Scripting/-/archive/master/Shell_Scripting-master.tar
 - https://tinyurl.com/yckrz9pc

This will download a tar file of the archive. Use the tar command to create a folder with all the files. Or use the commands below


```bash
wget https://tinyurl.com/yckrz9pc -O Shell_Scripting.tar

tar -x -f Shell_Scripting-master.tar
```

## Prepare editor
Insure you have your editor working. I will be using Atom, but any text editor
will work.

## Open terminal window
Pick your favorite terminal window to open.
