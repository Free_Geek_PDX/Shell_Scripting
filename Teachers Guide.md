# Teachers Guide for Shell Scripting

## Class description:
Learn how to write shell scripts to control your computer, make your own programs, and make timed actions. You will need to have knowledge of the command line.

## Goal of class:
By the end of this class, students will be able to write their own shell scripts to do simple activities, and know how to learn more to make more complicated scripts.

## Ideal class audience:
Any Linux user who wants to do more with their computer.

## Required experience:
Working knowledge of computers and some typing skills.
- Total runtime: 2 hours
- Maximum number of students: 8
- Materials/equipment needed:
- Laptops with Atom (a text editor) and access to the web for resources.
- Location: Free Geek classroom

## Introduction (5 minutes) [MANDATORY]
- Before you class begins, please be sure to introduce yourself.
Tell your students your name, your role at Free Geek, and any
interesting or relevant background experience you have.
- Ask your students to introduce themselves: their name, their
relationship to Free Geek, and their interest in/goals for this class.
- Ask your students to silence their cellphones and be courteous to
one another.
- Encourage them to ask questions whenever you haven’t made yourself
clear, but also ask them to write down questions that are more specific
or that only apply to themselves, and ask them after class.

# Core Concepts (2 minutes) [MANDATORY]
Today’s topics:
- Review of the command line
- Elements of a script
- Variables
- Calculations
- If-else-then decisions
- Comparison operations
- Loops; for, while, until
- Case statements
- Functions
- Using other programs in scripts


It is not the intent that this should leave the attendee fully able to write scripts but they should be able to take the things learned along with the resources provided and begin writing scripts.

Examples of all of the items covered will be provided so that the students have examples. There will be some hands-on work but with the limited time not as much as might be desired.

# Tools used Today (<5 minutes:10 minutes)
To be able to do some of the examples and learn on your own we are using the following tools today.
- `gnome-terminal` or other terminal that you have on your system.
- `Atom` text editor. It can be found at [https://atom.io](https://atom.io/)
Any text editor can be used but this one has nice syntax highlighting, folder views, and a wide variety of special plusgins for use.
> Have the students open the applications and confirm

# Body of Lesson
## Review of the Command Line (<5 minutes: 15)
The special
 characters are:

**metacharacter**

A  character  that,  when unquoted, separates words.  One of the following:

`|  & ; ( ) < > <space> <tab> <newline>`

**control operator**

A token that performs a control function.  It is one of the following symbols:

`|| & && ; ;; ;& ;;& ( ) | |& <newline>`

**common usages**

- `\`  –  used to indicate a continuation of the command line on the next line. Used to make scripts and commands legible
- `&`  –  indicates that this is the end of a line and the command is executed in the background
- `>`  –  redirect output to whatever follows, a file descriptor
- `<`  –  redirect input from whatever follows, a file descriptor
- `>>`  –  redirect output and append to file that follows, not useful for commands
- `2>`  –  redirect error output to whatever follows, can also use &>=
- `|`  –  send output to input of the next command
- `|&`  –  send output and error to input of next command

> Open command-line.sh
Command line characteristics, options, getting help, etc.
> Special characters, &, &&, |, #, $, and \ are very important  
> Return values from operations, success and failure of a command

> Open `command-line.sh` and discuss various examples  
> **First example** - shows success, throws output away using redirection `> null`  
> - Return value is 0 for success, non-zero is failure of some kind  
> - Talk about redirection > and >> and 2>  

> **Second example** - shows failure, aging the output is thrown away, but the error output is not and is shown.  
> - Return value is 2 for failure and the number is given for the type of error.
> - Refer to `man ls` for meaning.

Three things returned from a command,
- return value
- standard output
- error output

> Let the users spend a few moments trying some of these things output

## Elements of a script (5 minutes) `20 `
The first element of the script is the first line that gives the path to the command that will run the script. This allows the operating system to figure out that the script is a shell script, python script, perl script, or some other script.

For shell scripts the following is used to invoke the bash shell.
```bash
#!/bin/bash
```
This is the path to the program that interprets the script. You can use multiple different languages.

> show the ```hello.groovy``` and ```hello.py``` scripts

There are two ways to run a script.
1. Run the command by typing the following:
```bash
bash <script>
```
where the ```<script>``` is the name of the script.

2. Change the permissions on the file so that it is executable.  
Then you can run the script by inputting the path to the script followed by the script name.
```bash
~/scripts/<script>
```
You can use the file explorer and right-click on the file and select permissions and give the file the execute permission.  
Or you can use the command
```BASH
chmod u+x <script>
```
to change the permission of the script and give it execute.

> Discuss permissions, user, group, and other. Discuss briefly ```chmod```  
> Discuss why the path has to be used, unless you add your directory to the path variable. Show how that is done.  

```bash
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
```

The name of the script is not important. It is nice to end in .sh so that looking at a listing will tell you what it is but that is not essential.

## Variables and Parameters (5 minutes) `25`
A variable is the name of something that holds data. Referencing its value is called variable substitution.

> Open `variables.sh`

Variables are assigned with an `=` sign with no spaces around it.

< Scope of variables, they are local to the script or terminal running them but can be exported. Also variables are untyped but you can declare them to get limited types.

> Open `parameters.sh`

Parameters are the items on the command line that you use to invoke the script.

## Using commands in scripts (10 minutes) `35`
The power of the shell script and linux is the ability to use commands to get information and to interact with the system.

Commands can be used to set variables.
> open commands-2.sh  
> briefly discuss the actions in the script, have the users try this

## Calculations and other tips
### Curly braces
1) Array builder `{0..10} {0..10..2} {a..m..3} {0..4}{0..4}`
2) Access elements of an array (see curly.sh)
3) Cutting a chunk out of a string `${$var%str}` to cut str from the end of var and `${var#str}` to cut str from the beginning of var.

### Parenthesis
1) `(...)` is a command that starts a new subshell in which shell commands are run. Note that the (  ) needs to have spaces around it as it is a reserved word.
2) $(...) is an expression that starts a new subshell, whose expansion is the standard output produced by the commands it runs.
This is similar to another command/expression pair in bash: \`cmd\`
3) ((...)) is an arithmetic statement
4) $((...)) is an arithmetic expression. `echo $((3+4))`
### Square brackets
1) `$[ Expression ]` calculates expression `echo $[12*30]`
2) `[ expression ]` does a test, expression can be anything, see below
3) `[[ expression ]]` is another form of test that is built in (see below)


## tests

There are a number of different ways to create tests. Tests in BASH are based on the return value of a command. The standard way is to use the `test` command.
> `man test` and discuss the various tests that are available.
> look at testexam.sh

The second way is to use the shortcut `[ ... ]` instead of `test`. All the same tests are available.

The third way is to use the BASH builtin test of `[[ ... ]]` which acts much like the `test` command but uses a builtin function rather than a program.

The fourth way is to use a command and let the return value of the command function as a test.

Finally, you can use a calculation `$(( 4 > 3 ))` or `$[ 4 > 3]``

## If-else-then decisions (10 minutes) `45`
The if/then tests the **exit status** of a list of commands and if it is 0, then it executes the **then** part.  
- A left bracket **[** is a synonym for **test** and considers its operators as comparison expressions., 0 for true, 1 for false. Look at man test to see what it can test.
- If the special **built in** operator **[[...]]** is used this is an extended test command and works similar to other languages
- **((..))** and **let** constructs return an exit status. This construct is useful for manipulating variables

> open tests.sh  
> Discuss the examples shown

**syntax**  
```bash
if <test>
then
  <commands>
else
  <commands>
fi
```
The **else** and the commands after it are optional. the **fi** is not.

There is also the use of `elif` which functions like and else and if forming a nested if statement
> have the students try this out in their test script  
>
## Case statement
syntax

```bash
case expr in
  pattern1 )
        statements ;;
    pattern2 )
        statements ;;
    ...
esac
```


> Open case.sh

* Case statement first expands the expression and tries to match it against each pattern.
* When a match is found all of the associated statements until the double semicolon (;;) are executed.
* After the first match, case terminates with the exit status of the last command that was executed.
* If there is no match, exit status of case is zero.

### Break for 10 minutes:`55`

## Loops; for, while, until (10 minutes)
**for loops**

```bash
for arg in [list]
do
  <commands>
done
# or
for arg in {n..m}
do
  <commands>
done
```

Another example
```bash
for (( c=1; c<=5; c++ ))
do
 echo "$c"
done
```

or on one line

```bash
for (( c=1; c<=5; c++ ));  do   echo "$c" ;  done
```

> open script loops.sh

**while**

```bash
while [ condition ]
do
  <commands>
done
```

Note that the [] is the same as in tests. You can also use the more general [[...]] syntax to do the test. The [] is not necessary

> open loops2.sh  
> discuss the while loop and its usage

**until**
> open loops-until.sh

```bash
until [ condition ]
do
  <commands>
done
```

**break** and **continue** in loops

> open loops3.sh
> discuss how **continue** skips the rest of the commands and loops
> and **break** stops the loop and continues right after the loop

## Functions (10 minutes)
Bash has functions but they are limited. A function is code block that implements operations. When you have repetitive code with only slight variations they use a function.

**syntax**
```bash
function function_name {
  commands
}
```

> the { } may appear in separate lines. the () after the function name is optional but should be included if just for similarity to other languages.  
>The function definition must precede its usage.

The function can be called like any command. You can include parameters in the command line that are separate from the parameters passed into the script. The parameters are accessed in the same way as the command line.

> open functions.sh  
> discuss the examples  
> not the return provides and exit status for the function

## GUI ##
zenity is one of several programs for making dialogs using scripts.
> open zenity.md for description

> ope n zenity.sc for example

## Examples (10 minutes)
### name.sh
### touchpad.sh
### profile.sh
### postgres-backup.sh

## Question and Answer (10 minutes)

> Let the students ask questions and try things at their computer so they can learn.
> If there are specific things not covered here that come up, answer as best you can
> and then redirect them to resources.

# Summary (2 minutes)
> - Review this core concepts of today’s lesson.
- Offer useful tips and additional resources for further learning.

# Closing (2 minutes)

>- REFER EACH STUDENT TO THE FREE GEEK SURVEY (printed copies available in the classroom)
- Encourage students to take other classes at Free Geek, especially if they
dovetail with the lessons you taught today.
- Thank them!
