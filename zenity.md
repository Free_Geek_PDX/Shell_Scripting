# zenity

The possible options are (all preceded by --):

 * calendar
 * entry
 * error
 * file-selection
 * info
 * list
 * notification
 * progress
 * question
 * text-info
 * warning
 * scale
 * color-selection
 * password
 * forms
### General options
 * --title=TITLE
 * --window-icon=ICONPATH
 * --width=WIDTH 
 * --height=HEIGHT
 * --timeout=TIMEOUT
### Calendar options
 * --text=STRING
 * --day=INT
 * --month=INT
 * --year=INT
 * --date-format=PATERN
### Text entry options
 * --text=STRING
 *       --entry-text=STRING
 *       --hide-text

###       Error options
 *     --text=STRING
 *       --no-wrap
 *       --no-markup

###  File selection options

 *  --filename=FILENAME
 *  --multiple
 *  --directory
 *  --save Activate save mode
 *  --separator=SEPARATOR
 *  --confirm-overwrite
 * --file-filter=NAME | PATTERN1 PATTERN2

###  Info options
 *  --text=STRING
 *  --no-wrap
 *  --no-markup

### List options
 *       --text=STRING
 *       --column=STRING
 *       --checklist
 *       --radiolist
 *       --separator=STRING
 *       --multiple
 *       --editable
 * --print-column=NUMBER
 *   --hide-column=NUMBER
 *   --hide-header

###        Notification options
*     --text=STRING
*       --listen
              Listen  for  commands  on  stdin.  Commands  include  'message',
              'tooltip', 'icon', and 'visible' separated by a colon. For exam‐
              ple,  'message:  Hello  world',  'visible:  false',  or   'icon:
              /path/to/icon'.  The  icon  command  also accepts the four stock
              icon: 'error', 'info', 'question', and 'warning'

###       Progress options
*       --text=STRING
*       --percentage=INT
*       --auto-close
*       --auto-kill
*       --pulsate
*       --no-cancel

###       Progress options
*       --text=STRING
*       --percentage=INT
*       --auto-close
*       --auto-kill
*       --pulsate
*       --no-cancel

###      Text options
* --filename=FILENAME
*       --editable
*       --checkbox=TEXT
*       --ok-label
*       --cancel-label

###       Warning options
*       --text=STRING
*       --no-wrap
*       --no-markup

###  Scale options
*   --text=STRING
*       --value=VALUE
*       --min-value=VALUE
*       --max-value=VALUE
*       --step=VALUE
*       --print-partial
*       --hide-value

###      Color selection options
*       --color=VALUE
*       --show-palette

###      Forms dialog options
*       --add-entry=FIELDNAME
*       --add-password=FIELDNAME
*       --add-calendar=FIELDNAME
*       --text=STRING
*       --separator=STRING
*       --forms-date-format=PATTERN
