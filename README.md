# Shell Scripting
This is the repository for the shell scripting class taught at Free Geek in Portland Oregon.

It consists of the following:

1.  [Student Handout](/Student Handout.md)
2.  [Teacher guide](/Teachers Guide.md)
3.  Example files in the directory `scripts`
