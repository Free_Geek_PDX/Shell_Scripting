# Shell Scripting Student Handout
## Setup before class
To follow along with the scripts and to have them available later do either of the following:

 - `git clone https://gitlab.com/Free_Geek_PDX/Shell_Scripting.git`

 or

  `git clone http://bit.ly/FGScripting`

 or if you would rather do it without using git

 - https://gitlab.com/Free_Geek_PDX/Shell_Scripting/-/archive/master/Shell_Scripting-master.tar
 - https://tinyurl.com/yckrz9pc

 ```bash
 wget https://tinyurl.com/yckrz9pc -O Shell_Scripting.tar

 tar -x -f Shell_Scripting-master.tar
 ```

 ## REVIEW OF COMMAND LINE

 ## ELEMENTS OF SCRIPTS
 ### First line

 First line of a script file points to the program to use to run the script. For BASH scripts, use


 `#!/bin/bash`

Here is a simple program
 ```bash
 ## Simple program
 #!/bin/bash
 echo Hello World
 ```
 ## RUNNING SCRIPTS
 There are two methods of running scripts. You can execute a script by using bash in front of the filename of the script followed by the arguments.

 `bash filename [arguments]`

 The other method is to change the execute bit of the file using `chmod`

`chmod u+x filename`

 Which makes the filename executable by the user. If you want group or everyone to be able to execute the file then use g or o instead of u.

 Then the file can be executed by entering the filename. Unless the file is in the PATH variable then you will need to specify the location of the file. If you are executing it in the same directory you are in you will need to preface the name with ./ to indicate the current directory.

 -  `./filename [arguments]` if located in current directory
 - `/<path>/filename [arguments]` if not located in current directory
 - `filename [arguments]` if on the PATH

 ## Variables and Parameters
 A variable is the name of something that holds data. Referencing its value is called variable substitution.

 ## Using commands in scripts
 The power of the shell script and linux is the ability to use commands to get information and to interact with the system.

 ## If-else-then decisions
 The if/then tests the **exit status** of a list of commands and if it is 0, then it executes the **then** part.  
 - A left bracket **[** is a synonym for **test** and considers its operators as comparison expressions., 0 for true, 1 for false. Look at man test to see what it can test.
 - If the special **built in** operator **`[[...]]`** is used this is an extended test command and works similar to other languages
 - **`((..))`** and **`let`** constructs return an exit status. This construct is useful for manipulating variables

 ## Loops; for, while, until
 **for loops**

```bash
  for <var> in <list>
  do
   <commands>
  done
```
<list> can be  `{n..m}` to generate a list of numbers or letters

**while**

```bash
while [ condition ]
do
  <commands>
done
```
Note that the [] is the same as in tests. You can also use the more general [[...]] syntax to do the test. The [] is not necessary

**until**
```bash
until [ condition ]
do
  <commands>
done
```()

**break** stops the loop and **continue** goes to the next bottom for the next value

## Functions
Bash has functions but they are limited. A function is code block that implements operations. When you have repetitive code with only slight variations they use a function.

**syntax**
```bash
function_name () {
  commands
}
```

# RESOURCES
- https://www.cyberciti.biz/open-source/learning-bash-scripting-for-beginners/
List of resources for beginning scripters
- http://tldp.org/LDP/abs/html/index.html Advanced concepts for Scripting
- https://gitlab.com/Free_Geek_PDX/Shell_Scripting/tree/master Current course
- https://www.tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html
