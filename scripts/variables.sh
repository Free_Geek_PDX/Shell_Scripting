#!/bin/bash
# This shows how variables Work
var=723
echo var
echo $var

# To include white space use quotes
var="Hello scripters"
echo "var = \"$var\""

echo
echo "Indirect --"
a=a_variable
a_variable="different value"

echo "a = $a"           # Direct reference.

echo "indirect a = ${!a}"    # Indirect reference.

echo

# assigns results from commands
nvar=`echo Great!`
echo 'echo $nvar - ' $nvar

# some system variables
echo
echo "System variables"
echo '$HOME' $HOME
echo '$PATH' $PATH
echo '$USER' $USER
echo '$PWD' $PWD

# To see all variables including bash variables
( set -o posix ; set ) | less

exit 0
