#!/bin/bash

month=("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
# print an element in the array month
echo ${month[3]}

#Combine all of these together
letters=({a..z}{a..z})

# Parameter expansion
echo ${letters[5]}

#Cutting a part of the string
a="A long string"
echo ${a%ing}

pwd
# change file extensions
i=../byron.jpg
convert $i ${i%jpg}png

return 0

# Example
for i in *.jpg; do convert $i ${i%jpg}png; done
