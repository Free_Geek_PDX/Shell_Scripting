#! /bin/bash
# backup a postgres database giving it a name for the current date
export PGPASSWORD="mysecret"
pg_dump --host localhost --port 5432 --username postgres gnucash1 \
 > backups/backup-$(date +%Y-%m-%d).sql
