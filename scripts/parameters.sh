#!/bin/bash
#
# Parameters
#
MINPARAMS=3

echo -------- starting ---------
echo "Script name is \"$0\"."
echo "The name is \"`basename $0`\"."
# basename strips off the directory information

# $# is the number of parameters passed in
echo "Number of parameters $#"

# Each parameter is obtained by putting a $ in front
echo "Parameter 1 is $1"
echo "Parameter 2 is $2"
echo "Parameter 3 is $3"
echo "Parameter 4 is $4"

# note that after 9, you will need to put the number in brackets {10}

exit 0
