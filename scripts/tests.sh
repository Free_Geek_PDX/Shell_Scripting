#!/bin/bash
# zero test-z
if [ -z $1 ]
then
  echo "No command line arguments"
else
  echo "First command line argument=$1"
fi

# exists test
filenm=$1
if [ -e $filenm ]
then
  echo "file $filenm exists"
else
  echo "file $filenm does not exist"
fi

num1=1
num2=2
# use [[...]] construct for arithmetic operations
if [[ num1 -lt num2 ]]
then
  echo "$num1 < $num2"
fi

if [[ num1 -ge num2 ]]
then
  echo "$num1 > $num2"
else
  echo "$num2 > $num1"
fi


echo "let command"
# exit status
let "num = (( 0 && 1 ))"
echo Exit status $? # 1 (exit status)
echo Evaluation $num # 0 (result of evaluation)
