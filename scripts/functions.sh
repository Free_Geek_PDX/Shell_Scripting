#!/bin/bash
#
# functions

func () {
  n=`seq $#`
  for var in $n
  do
    echo "$var = $1"
    shift
  done
  return 2
}

func $1 p1 p2 p3

echo "Exit status $?"
