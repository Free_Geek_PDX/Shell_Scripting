#!/bin/bash
# 1 Getting the return code
ls -al > /dev/null
echo return code \"ls -al\" $?
# talk about redirection of output

# 2 failure in return code
ls Bingo > /dev/null
echo return code \"ls bingo\" $?

# refer to man ls for menaing of error codes
