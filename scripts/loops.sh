#!/bin/bash

# simple for loops
for lvar in "One" "Two" "Three" "Four"
do
  echo $lvar
done

echo
echo "Again..."
for lvar in "one two three four five" ; do
  echo $lvar
done
# the list is really only one value with spaces
echo
echo "Again... again..."
loopv="one two three four five"
for lvar in  $loopv ; do
  echo $lvar
done

echo ; echo "Planets"
for planet in Mercury Venus Earth Mars Jupiter Saturn Uranus Neptune Pluto
do
  echo $planet
done
# this works as long as there are no spaces

echo ; echo "Listing"
for  fvar in scripts/*sh
do
  echo $fvar
done

echo ; echo "Sequences"
loopv=`seq 10 15`
for lvar in $loopv
do
  echo $lvar
done

echo ; echo "Brace expansion"
for lvar in {4..8}
do
  echo $lvar
done
