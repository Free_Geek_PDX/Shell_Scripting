#!/bin/bash

# break and continue
nmax=10
n=0

echo
echo "print numbers from 1 to 10 but exclude 4 and 6, stop after 8"
while (( n < nmax ))
do
  n=$(($n+1))
  if [ $n -eq 4 ] || [ $n -eq 6 ]
  then
    continue
  fi
  if [ $n -ge 9 ]
  then
    break
  fi 
  echo -n "$n "
done
echo
