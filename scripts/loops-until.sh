#!/bin/bash

# Until loop
read -p "Who is logged into the computer " name
until [ $name = $USER ]
do
  echo "The logged in user is not $name but $USER"
  echo "Try again..."
  echo "----------------------------"
  read -p "Who is logged into the computer " name
done

echo "Congratulations, you have entered the user name correctly"
