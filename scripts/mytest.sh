#!/bin/bash
echo "|$1|"

if [ $1 ]
then
  if [[ $1 = $USER ]]
  then
    echo "Hello $1"
  else
    echo "Are you really $1?"
  fi
else
  echo "Enter your name on the command line"
fi
