#!/bin/bash

# while loop
nmax=5
n=2
echo "loop from $n to $nmax"
while [ "$n" -le "$nmax" ]
do
  echo -n "$n " # -n to suppress newline
  n=$(($n+1))
done
echo
# try different ways to add to n
n=`expr $n + 1`
echo $n
# why was the value one more than the last one printed?
let "n +=1"
echo $n
let "n++"
echo $n
((n++))
echo $n
