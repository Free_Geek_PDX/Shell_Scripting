#!/bin/bash
dotouch() {
  # xl=`xinput list | grep pointer`
  msid=
  tpid=
  xinput list | grep pointer > tmp.txt

  virt=1
  while read -r line
  do
    if grep "Virtual" <<< $line
      then
        virt=0
      else
      #  echo "-R- $line"
        if grep "TouchPad" <<< $line
        then
          # echo "*** Touchpad ***"
          if [[ $line =~ id=([0-9]*) ]]
          then
            # echo ${BASH_REMATCH[1]}
            tpid=${BASH_REMATCH[1]}
            # echo "TP ID = $tpid"
          else
            echo " ######### No match"
          fi
          else
            # echo "*** mouse ***"
            if [[ $line =~ id=([0-9]*) ]]
            then
              msid=${BASH_REMATCH[1]}
              # echo "Mouse id = $msid"
            else
              echo " ######### Failed mouse ID"
            fi
        fi
    fi
  done < tmp.txt

  # get the enabled of touchpad
  line=`xinput list-props "$tpid" | grep "Device Enabled"`
  if [[ $line =~ ([0-9])$ ]]
  then
    tpenable=${BASH_REMATCH[1]}
  else
    tpenable=${BASH_REMATCH[1]}
    echo "No match for device enabled"
  fi
  echo "TP enable $line ---- $tpenable ms $msid tp $tpid"

  # echo "Do the work now $tpid & $msid"

  if [ -z $msid ]
  then
    # echo "No msid $msid"
    if [ $tpenable -eq 0 ]
    then
      # echo "Setting touchpad 1"
      xinput set-prop $tpid "Device Enabled" 1
      echo "Enabled touchpad"
    fi
  else
    # echo "Yes, msid $msid"
    if [ $tpenable -eq 1 ]
    then
      xinput set-prop $tpid "Device Enabled" 0
      echo "Disabled touchpad"
    fi
  fi
  rm tmp.txt
}

while dotouch
do
  # dotouch
  sleep 5
  echo "."
done
