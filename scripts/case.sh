#!/bin/bash

if [ $# -lt 2 ]
then
  echo "Usage " $0 command name
  exit 2
fi


case "$1" in

  1) echo "Hello $2"
  ;;

  2) echo "Goodbye $2"
  ;;

# this matches any input
  *) echo "command is not 1 or 2"
  echo "Usage " $0 command name
  exit 1
  ;;
esac
