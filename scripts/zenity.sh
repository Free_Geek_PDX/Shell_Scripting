#!/bin/bash

# display a question so user can continue or quit
zenity   --question  --title  "Alert"  --text "Microsoft Windows
              has been found! Would you like to remove it?"
ans=$?
if [ $ans -eq 0 ]
then
  zenity --info --title "Your answer" \
    --text="You have selected to remove Windows"
else
    zenity --info --title "Your answer" \
      --text="You have selected to keep Windows for some silly reason"
fi

find  .  -name  '*.sh'  |  zenity --list --title "Search Results" \
              --text "Finding all script files.." --column "Files"
